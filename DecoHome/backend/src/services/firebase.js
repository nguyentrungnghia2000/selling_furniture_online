const { initializeApp, applicationDefault, cert } = require('firebase-admin/app');
const { getFirestore, Timestamp, FieldValue } = require('firebase-admin/firestore');
var admin = require("firebase-admin");

var serviceAccount = require("../app-demo-e7cf0-firebase-adminsdk-lyhko-8e64a9100d.json");

initializeApp({
    credential: cert(serviceAccount)
});

exports.database = getFirestore();