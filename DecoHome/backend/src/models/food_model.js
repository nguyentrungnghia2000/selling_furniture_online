const { send } = require('micro');
const { database } = require('../services/firebase');

module.exports = {
    addFood: async (req, res) => {
        const docRef = database.collection('foods').doc('alovelace');

        const result = await docRef.set({
            id: 1,
            name: 'hao hao',
            price: 2500,
            image: 'noodles.png'
        });

        send(res, 200, result);
    },

    getFood: async(req, res) => {
        let arr = [];
        const docRef = database.collection('foods');

        const result = await docRef.get();

        result.forEach((doc) => {
            arr = [...arr, {
                id: doc.data().id, 
                name: doc.data().name, 
                price: doc.data().price, 
                image: doc.data().image},];
        });

        return arr;
    }
}