const { send } = require('micro');
const { addFood, getFood } = require('../models/food_model');
const { router, get } = require('microrouter');
 
const notfound = (req, res) => send(res, 404, 'Not found route');
 
exports.route  = router(
    get('/food/add', addFood), 
    get('/food/get', getFood),
    get('/*', notfound)
);