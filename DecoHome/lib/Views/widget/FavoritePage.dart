import 'package:DecoHome/Views/models/product.model.dart';
import 'package:DecoHome/Views/services/product.service.dart';
import 'package:DecoHome/Views/widget/ProductListPage.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
//import 'package:google_nav_bar/google_nav_bar.dart';

class FavoritePage extends StatefulWidget {
  FavoritePage({Key key}) : super(key: key);
  @override
  _FavoriteState createState() => _FavoriteState();
}

class _FavoriteState extends State<FavoritePage> {
  @override
  Widget build(BuildContext context) {
    return StreamProvider<List<Product>>.value(
      value: ProductService().getAllProd,
      child: SafeArea(
          child: Container(
        padding: EdgeInsets.only(left: 10, top: 10, right: 10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(children: <Widget>[
              Expanded(
                  child: Text(
                "Favorites",
                style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
              )),
              //Expanded(child: Text(""), flex: 1),
              //Expanded(child: Text(""),flex:1),
              Icon(
                Icons.shopping_cart_rounded,
                color: Colors.black,
                size: 28,
              )
            ]),
            Padding(padding: EdgeInsets.only(top: 15, bottom: 15)),
            Container(padding: EdgeInsets.only(left: 10), child: ProductList()),
            //ItemCard(),
            // Expanded(
            //     child: Container(
            //   child: GridView.builder(
            //     itemCount: 3,
            //     gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            //         crossAxisCount: 2,
            //         crossAxisSpacing: 15,
            //         childAspectRatio: 0.75),
            //     itemBuilder: (context, index) => Text(),
            //   ),
            // )
            //     //   Padding(
            //     //     padding: const EdgeInsets.symmetric(horizontal: 0),
            //     //     child: ItemCard(),
            //     //   ),
            //     )
          ],
        ),
      )),
    );
  }
}
