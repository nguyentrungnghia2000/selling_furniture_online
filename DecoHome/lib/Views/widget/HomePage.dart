import 'package:DecoHome/Views/models/product.model.dart';
import 'package:DecoHome/Views/services/product.service.dart';
import 'package:DecoHome/Views/widget/ProductListPage.dart';
import 'package:flutter/material.dart';
import 'package:DecoHome/Views/widget/FavoritePage.dart';
import 'package:DecoHome/Views/widget/CartPage.dart';
import 'package:provider/provider.dart';
//import 'package:google_nav_bar/google_nav_bar.dart';

class HomePage extends StatelessWidget {
  HomePage();
  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    ProductService productService = new ProductService();
    return StreamProvider<List<Product>>.value(
      value: ProductService().getAllProd,
      child: Scaffold(
          body: SafeArea(
              child: Container(
                  child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Stack(
              children: <Widget>[
                Image(
                  image: AssetImage('Poster_1.jpg'),
                  fit: BoxFit.contain,
                  width: screenWidth,
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  height: 50,
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "Home",
                          style: TextStyle(
                              fontSize: 22, fontWeight: FontWeight.bold),
                        ),
                        //Expanded(child: Text(""), flex: 1),
                        //Expanded(child: Text(""),flex:1),
                        InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => CartPage(),
                                ));
                          },
                          child: Icon(
                            Icons.shopping_cart_rounded,
                            color: Colors.black,
                            size: 28,
                          ),
                        )
                      ]),
                ),
              ],
            ),
            Padding(
                padding: EdgeInsets.all(10),
                child: Text(
                  "Trending",
                  style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18),
                )),
            Container(padding: EdgeInsets.only(left: 10), child: ProductList()
                // Row(children: <Widget>[
                //   ItemCard(),
                //   Padding(padding: EdgeInsets.only(left: 10)),
                //   ItemCard(),
                //   Padding(padding: EdgeInsets.only(left: 10)),
                //   ItemCard(),
                // ]),
                ),
          ],
        ),
      )))),
    );
  }
}
