import 'package:DecoHome/Views/models/product.model.dart';
import 'package:DecoHome/Views/widget/ProductInfoPage.dart';
import 'package:flutter/material.dart';

class ItemCard extends StatelessWidget {
  final Product prod;
  final Image noImage = Image.asset("assets/Ghe_1.png");

  ItemCard({this.prod});
  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => ProductInfoPage(),
              ));
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 200,
              //width: 170,
              //child: Text("data"),
              child: Stack(
                alignment: AlignmentDirectional.center,
                children: <Widget>[
                  Image.network(
                    prod.pic,
                    loadingBuilder: (context, child, loadingProgress) =>
                        (loadingProgress == null)
                            ? child
                            : CircularProgressIndicator(),
                    errorBuilder: (context, error, stackTrace) => noImage,
                  ),
                  Positioned(
                      top: 7.5,
                      left: 7.5,
                      child: Container(
                          padding: EdgeInsets.all(5),
                          child: Icon(
                            Icons.favorite,
                            size: 25,
                            color: Theme.of(context).accentColor,
                          ),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(30))))
                ],
              ),
              decoration: BoxDecoration(
                  color: Colors.black.withOpacity(0.1),
                  borderRadius: BorderRadius.circular(15)),
            ),
            Text(
              prod.name,
              style: TextStyle(fontWeight: FontWeight.w600),
            ),
            Text(prod.price.toString()),
          ],
        ));
  }
}
