import 'package:DecoHome/Views/models/product.model.dart';
import 'package:DecoHome/Views/widget/itemcard.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProductList extends StatefulWidget {
  @override
  _ProductListState createState() => _ProductListState();
}

class _ProductListState extends State<ProductList> {
  @override
  Widget build(BuildContext context) {
    final getAllProd = Provider.of<List<Product>>(context);

    return ListView.builder(
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      itemCount: getAllProd.length,
      itemBuilder: (context, index) {
        return ItemCard(prod: getAllProd[index]);
      },
    );
  }
}

// return StreamBuilder<QuerySnapshot>(
//     stream: FirebaseFirestore.instance.collection('Product').snapshots(),
//     builder: (context, snapshot) {
//       final products = snapshot.data.docs;
//       for (var product in products) {
//         // print(product.data());
//       }
//     });
