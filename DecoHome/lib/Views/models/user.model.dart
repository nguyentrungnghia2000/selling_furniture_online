import 'package:cloud_firestore/cloud_firestore.dart';

class Users {
  String id;
  String name;
  String email;
  String password;
  String date;
  String phoneNum;
  String address;
  String nationality;
  String photoUrl;

  Users.formSnapShot(DocumentSnapshot snapshot) {
    Map data = snapshot.data();
    this.id = data["uid"];
    this.name = data["name"];
    this.email = data["email"];
    this.password = data["password"];
    this.date = data["date"];
    this.phoneNum = data["phoneNum"];
    this.address = data["address"];
    this.nationality = data["nationality"];
    this.photoUrl = data["photoUrl"];
  }
}
