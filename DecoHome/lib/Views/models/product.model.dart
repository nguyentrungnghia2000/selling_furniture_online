import 'package:DecoHome/Views/models/product-category.model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:DecoHome/Views/models/product-category.model.dart';

class Product {
  String proid;
  String name;
  String manufacture;
  String prodInfo;
  num amount;
  num price;
  num rate;
  String pic;
  String prodCategory;

  Product.formSnapShot(DocumentSnapshot snapshot) {
    Map data = snapshot.data();
    this.proid = data["proid"] ?? '';
    this.name = data["name"] ?? '';
    this.manufacture = data["manufacture"] ?? '';
    this.prodInfo = data["prodInfo"] ?? '';
    this.amount = data["amount"] ?? 0;
    this.price = data["price"] ?? 0;
    this.rate = data["rate"] ?? 0;
    this.pic = data["pic"] ?? '';
    this.prodCategory = data["typeid"] ?? '';
  }
}
