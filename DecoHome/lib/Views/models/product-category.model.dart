import 'package:cloud_firestore/cloud_firestore.dart';

class ProductCategory {
  String typeid;
  String name;

  ProductCategory.formSnapShot(DocumentSnapshot snapshot) {
    Map data = snapshot.data();
    this.typeid = data["typeid"];
    this.name = data["name"];
  }
}
