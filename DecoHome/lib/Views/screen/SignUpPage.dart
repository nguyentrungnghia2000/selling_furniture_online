import 'package:DecoHome/Views/services/sign.service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:DecoHome/Views/screen/SignInPage.dart';
//import 'package:transparent_image/transparent_image.dart';
//import 'dart:async';
//import 'package:flutter/services.dart';

class SignUp extends StatefulWidget {
  @override
  State<SignUp> createState() {
    return _SignUp();
  }
}

class _SignUp extends State<SignUp> {
  String name = '';
  String email = '';
  String password = '';
  String error = '';
  final SignService _auth = SignService();
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return SafeArea(
      child: Scaffold(
        //resizeToAvoidBottomInset: false,
        body: Center(
          child: ListView(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Container(
                    child: FlatButton(
                      padding: EdgeInsets.only(),
                      child: Icon(Icons.arrow_back_ios_rounded),
                      color: Colors.transparent,
                      minWidth: 50,
                      height: 50,
                      onPressed: () {
                        print('back to login');
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) =>
                                  SigninPage(), // back to login scene
                            ));
                      },
                    ),
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(left: 40),
                  ),
                  Stack(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(top: 20, left: 30),
                        child: FlatButton(
                          minWidth: 140,
                          height: 5,
                          color: Color.fromRGBO(6, 130, 130, 50),
                          child: Text(
                            '',
                            style: TextStyle(color: Colors.red),
                          ),
                          onPressed: () {},
                        ),
                      ),
                      Container(
                        decoration:
                            BoxDecoration(color: Color.fromARGB(0, 0, 0, 0)),
                        padding: EdgeInsets.only(),
                        child: Text(
                          'Sign up',
                          style: TextStyle(
                              fontSize: 42, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              Container(
                padding: EdgeInsets.only(bottom: 100, left: 40),
                child: Text(
                  'Create an account',
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
                ),
              ),
              Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(top: 10, left: 40, right: 40),
                        child: TextFormField(
                          validator: (val) => val.isEmpty
                              ? 'Please enter your name before sign up'
                              : null,
                          onChanged: (val) {
                            setState(() {
                              this.name = val;
                            });
                          },
                          decoration: InputDecoration(
                              border: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green)),
                              labelText: 'Name',
                              hintText: 'Enter your name',
                              hintStyle: TextStyle(fontSize: 13)),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 10, left: 40, right: 40),
                        child: TextFormField(
                          validator: (val) => val.isEmpty
                              ? 'Please enter your email before sign up'
                              : null,
                          onChanged: (val) {
                            setState(() {
                              this.email = val;
                            });
                          },
                          decoration: InputDecoration(
                              border: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green)),
                              labelText: 'Email',
                              hintText: 'Enter your email',
                              hintStyle: TextStyle(fontSize: 13)),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                            top: 10, bottom: 80, left: 40, right: 40),
                        child: TextFormField(
                          validator: (val) => val.length < 6
                              ? 'Password needs at least 6 charaters to login'
                              : null,
                          obscureText: true,
                          onChanged: (val) {
                            setState(() {
                              this.password = val;
                            });
                          },
                          decoration: InputDecoration(
                              border: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green)),
                              labelText: 'Password',
                              hintText: 'Enter your password',
                              hintStyle: TextStyle(fontSize: 13)),
                        ),
                      ),
                    ],
                  )),
              Container(
                  child: Align(
                child: SizedBox(
                  width: 240,
                  child: FlatButton(
                    height: 60,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30)),
                    child: Text(
                      'Sign up',
                      style: TextStyle(fontSize: 16),
                    ),
                    color: Color.fromRGBO(6, 130, 130, 1),
                    textColor: Colors.white,
                    onPressed: () async {
                      if (_formKey.currentState.validate()) {
                        print(this.email);
                        print(this.password);
                        dynamic result =
                            await _auth.SignUp(name, this.email, this.password);
                        if (result == null) {
                          setState(() {
                            error = 'Register unsuccess';
                          });
                        } else {
                          name = '';
                          email = '';
                          password = '';
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => SigninPage(),
                              ));
                          setState(() {
                            error = 'Register success';
                          });
                        }
                      }
                    },
                  ),
                ),
              )),
              Container(
                padding: EdgeInsets.only(left: 65, top: 10),
                child: Text(
                  'By creating an account, you agreed with our',
                  style: TextStyle(fontSize: 13),
                ),
              ),
              Container(
                padding: EdgeInsets.only(top: 10),
                child: Row(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(left: 90),
                      child: Text(
                        'Terms of Use',
                        style: TextStyle(
                            fontSize: 13,
                            decoration: TextDecoration.underline,
                            color: Color.fromRGBO(6, 130, 130, 1)),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 10),
                      child: Text(
                        'and',
                        style: TextStyle(
                          fontSize: 13,
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 10),
                      child: Text(
                        'Privacy Policy',
                        style: TextStyle(
                            fontSize: 13,
                            decoration: TextDecoration.underline,
                            color: Color.fromRGBO(6, 130, 130, 1)),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
