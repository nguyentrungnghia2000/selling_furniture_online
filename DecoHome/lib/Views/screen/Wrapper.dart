import 'package:DecoHome/Views/models/account.model.dart';
import 'package:DecoHome/Views/screen/MainPage.dart';
import 'package:DecoHome/Views/screen/SignInPage.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<Account>(context);

    if (user != null) {
      return MainPage();
    } else {
      return SigninPage();
    }
  }
}
