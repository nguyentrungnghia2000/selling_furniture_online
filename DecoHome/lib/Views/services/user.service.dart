import 'package:cloud_firestore/cloud_firestore.dart';

class UserService {
  final String uid;
  UserService({this.uid});
  final CollectionReference userCollection =
      FirebaseFirestore.instance.collection('Users');
  bool _isExist = false;

  Future updateUser(
      String id,
      String name,
      String email,
      String password,
      String date,
      String phoneNum,
      String address,
      String nationality,
      String photoUrl) async {
    return await userCollection.doc(uid).set({
      'id': id,
      'name': name,
      'email': email,
      'password': password,
      'date': date,
      'phoneNum': phoneNum,
      'address': address,
      'nationality': nationality,
      'photoUrl': photoUrl
    });
  }

  Stream<QuerySnapshot> get getUsers {
    return userCollection.snapshots();
  }

  bool CheckIdExist(String uid) {
    userCollection.doc(uid).get().then((data) => {
          if (data == null) {setIsExist(false)} else setIsExist(true)
        });
    return _isExist;
  }

  setIsExist(bool val) {
    this._isExist = val;
  }
}
