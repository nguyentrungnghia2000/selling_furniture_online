import 'package:DecoHome/Views/models/product.model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class ProductService {
  final CollectionReference prodCollection =
      FirebaseFirestore.instance.collection('Product');

  //update product
  Future updateProduct(
      String prodid,
      String name,
      String manufacture,
      String proInfo,
      num amount,
      num price,
      num rate,
      String pic,
      String prodCategory) async {
    return await prodCollection.doc(prodid).set({
      'prodid': prodid,
      'name': name,
      'manufacture': manufacture,
      'prodInfo': proInfo,
      'amount': amount,
      'price': price,
      'rate': rate,
      'pic': pic,
      'typeid': prodCategory
    });
  }

  //get all product
  List<Product> _prodList(QuerySnapshot snapshot) {
    return snapshot.docs.map((e) {
      return Product.formSnapShot(e);
    }).toList();
  }

  Stream<List<Product>> get getAllProd {
    return prodCollection.snapshots().map(_prodList);
  }
}
