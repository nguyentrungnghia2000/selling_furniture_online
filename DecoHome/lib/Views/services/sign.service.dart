import 'dart:math';

import 'package:DecoHome/Views/models/account.model.dart';
import 'package:DecoHome/Views/services/user.service.dart';
import 'package:DecoHome/configs/initialization.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:DecoHome/Views/models/user.model.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

class SignService {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  Status _status = Status.Uninitialized;
  final CollectionReference userCollection =
      FirebaseFirestore.instance.collection('users');

  Users _userData;
  Users get getUserData => _userData;

  Account _account;
  Account get getAccountData => _account;

  // create user obj based on firebase user
  Account _userFromFirebaseUser(User user) {
    return user != null ? Account(uid: user.uid) : null;
  }

  // auth change user stream
  Stream<Account> get user {
    return _auth
        .authStateChanges()
        //.map((FirebaseUser user) => _userFromFirebaseUser(user));
        .map(_userFromFirebaseUser);
  }

  //set status
  Future<void> _onStateChanged(User user) async {
    if (user == null) {
      _status = Status.Unauthenticated;
    } else {
      _status = Status.Authenticated;
    }
  }

  //sign in
  Future SignIn(String email, String password) async {
    try {
      UserCredential result = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      User firebaseUser = result.user;
      return _userFromFirebaseUser(firebaseUser);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  //sign up
  Future SignUp(String name, String email, String password) async {
    try {
      UserCredential result = await _auth.createUserWithEmailAndPassword(
          email: email, password: password);
      User firebaseUser = result.user;

      //get current date
      DateTime now = new DateTime.now();
      DateTime date = new DateTime(now.year, now.month, now.day);

      //create a new document for the user with uid
      await UserService(uid: firebaseUser.uid).updateUser(firebaseUser.uid,
          name, email, password, date.toIso8601String(), '', '', '', '');

      return _userFromFirebaseUser(firebaseUser);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  //sign out
  Future signOut() async {
    try {
      return await _auth.signOut();
    } catch (e) {
      return null;
    }
  }
}
