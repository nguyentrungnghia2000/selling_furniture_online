import 'package:DecoHome/Views/models/account.model.dart';
import 'package:DecoHome/Views/models/user.model.dart';
import 'package:DecoHome/Views/screen/SignInPage.dart';
import 'package:DecoHome/Views/screen/Wrapper.dart';
import 'package:DecoHome/Views/services/sign.service.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamProvider<Account>.value(
      value: SignService().user,
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: "DecoHome",
        home: Wrapper(),
        theme: ThemeData(
            accentColor: Color.fromRGBO(6, 130, 130, 50),
            hintColor: Color.fromRGBO(6, 130, 130, 0.3),
            hoverColor: Color.fromRGBO(6, 130, 130, 0.5)),
      ),
    );
  }
}
